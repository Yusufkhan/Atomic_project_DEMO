
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: #f1f1f1}

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>



<h2>Hoverable Dropdown</h2>


<div class="dropdown">
    <button class="dropbtn">Birthdate</button>
    <div class="dropdown-content">
        <a href="../Birthdate/create.php">Create</a>
        <a href="../Birthdate/index.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Textarea</button>
    <div class="dropdown-content">
        <a href="../Textarea/create.php">Create</a>
        <a href="../Textarea/create.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Book Title</button>
    <div class="dropdown-content">
        <a href="../book_title/create.php">Create</a>
        <a href="../book_title/index.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Gender</button>
    <div class="dropdown-content">
        <a href="../Gender/create.php">Create</a>
        <a href="../Gender/index.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">Hobby</button>
    <div class="dropdown-content">
        <a href="../Hobby/create.php">Create</a>
        <a href="../Hobby/index.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">ProfilePicture</button>
    <div class="dropdown-content">
        <a href="../ProfilePicture/create.php">Create</a>
        <a href="../ProfilePicture/index.php">Index</a>
    </div>
</div>
<div class="dropdown">
    <button class="dropbtn">City</button>
    <div class="dropdown-content">
        <a href="../City/create.php">Create</a>
        <a href="../city/index.php">Index</a>
    </div>
</div>


</body>
</html>
