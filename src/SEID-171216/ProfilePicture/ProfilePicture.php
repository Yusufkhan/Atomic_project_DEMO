<?php

namespace App\ProfilePicture;

use App\Message\Message;

use App\Models\Database;
use PDO;

class ProfilePicture extends Database
{
    public $id;
    public $name;
    public $profilePicture;
    public function setImage($image)
    {
        if (array_key_exists("id",$image))
        {
            $this->id = $image['id'];
        }

        if (array_key_exists("name",$image))
        {
            $this->name = $image['name'];
        }

        if (array_key_exists("profilePicture",$image))
        {
            $this->profilePicture = $image['profilePicture'];
        }
    }

    public function store()
    {
        $name=$this->name;
        $profilePicture=$this->profilePicture;

        $sql="INSERT INTO `profile_pictures`(`name`,`profile_picture`) VALUES (?,?)";

        $dataArray=array($name,$profilePicture);

        $get=$this->conn->prepare($sql);

        $res = $get->execute($dataArray);


        if ($res)
        {
            Message::message("Success");
        }
        else
        {
            Message::message("Failed ..");
        }




    }

    ///////////


    public function index()
    {
        $mysql="SELECT * FROM profile_pictures";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
        // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM profile_pictures WHERE id=".$this->id;
        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        //Utility::dd($read);
        return $singleData;

    }

    public function update()
    {
        $name=$this->name;

        $profile_pictures=$this->profilePicture;

        $mysql="UPDATE profile_pictures SET name = ?, profile_picture = ? WHERE id =".$this->id;

        $dataArray=array($name,$profile_pictures);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

        if ($singleData)
        {
            Message::message("Data Updated successfully...! ");
        }
        else
        {
            Message::message("Faild to Update Data...!  ");
        }



    }

}