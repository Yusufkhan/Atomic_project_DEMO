<?php

namespace App\City;
use App\Message\Message;
use App\Models\Database;
use PDO;

class City extends Database
{
    public $Id;
    public $fname;
    public $lname;
    public $cityname;

    public function setCityname($cityname)
    {
       if (array_key_exists("id",$cityname))
       {
           $this->id = $cityname['id'];
       }

        if (array_key_exists("fname",$cityname))
        {
            $this->fname = $cityname['fname'];
        }

        if (array_key_exists("lname",$cityname))
        {
            $this->lname = $cityname['lname'];
        }

        if (array_key_exists("cityname",$cityname))
        {
            $this->cityname = $cityname['cityname'];
        }
        return $this;
    }
    public function store()
    {
       $fname=$this->fname;
       $lname=$this->lname;
       $cityname=$this->cityname;


       $sql="INSERT INTO `city_name` (`first_name`, `last_name`, `city_name`) VALUES ('$fname', '$lname', '$cityname')";

       $result=$this->conn->exec($sql);
       if ($result)
       {
           Message::message("Successfully Inserted");
       }
       else
       {
           Message::message("Sorry !!! try Again..");
       }
    }

    public function index()
    {
        $mysql="SELECT  * FROM city_name";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
        // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM city_name WHERE id=".$this->id;
        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        //Utility::dd($read);
        return $singleData;

    }

    public function update()
    {
        $fname=$this->fname;
        $lname=$this->lname;
        $cityname=$this->cityname;


        $mysql="UPDATE `city_name` SET  `first_name` = ?, `last_name` = ?, `city_name` = ? WHERE `id` = ?".$this->id;

        $dataArray=array($fname,$lname,$cityname);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

       if ($singleData)
       {
           Message::message("Success");
       }else
       {
           Message::message("Error");
       }

    }
}