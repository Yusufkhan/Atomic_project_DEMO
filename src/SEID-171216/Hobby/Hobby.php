<?php

namespace App\Hobby;

use App\Message\Message;

use App\Models\Database;
use App\Utility\Utility;
use PDO;

class Hobby extends Database
{
    public $id;
    public $name;
    public $roll;
    public $hobby;


    public function setHobby($hobby)
    {
        if (array_key_exists("id",$hobby))
        {
            $this->id=$hobby['id'];
        }
        if (array_key_exists("Name",$hobby))
        {
            $this->name=$hobby['Name'];
        }

        if (array_key_exists("roll",$hobby))
        {
            $this->roll=$hobby['roll'];
        }

        if (array_key_exists("hobby",$hobby))
        {
            $this->hobby=$hobby['hobby'];
        }
        return $this;

    }

    public function store()
    {
        $name=$this->name;
        $roll=$this->roll;
        $hobby=$this->hobby;


        $mysql="INSERT INTO `hobby` (`name`, `roll`, `hobby`) VALUES (?,?,?)";
        $dataArray=array($name,$roll,$hobby);
        $get=$this->conn->prepare($mysql);
        $result=$get->execute($dataArray);
        if ($result)
        {
            Message::message("Data Inserted Successfully");
        }
        else
        {
            Message::message("Failed to insert data");
        }

    }

    ///////////

    public function index()
    {
        $mysql="SELECT  * FROM hobby";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
        // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM hobby WHERE id=".$this->id;

        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        // Utility::dd($mysql);
        return $singleData;

    }

    public function update()
    {
       $hobby=$this->hobby;
        $name=$this->name;
        $roll=$this->roll;

        $mysql="UPDATE `hobby` SET `name` = ?, `roll` = ? ,`hobby`=? WHERE `id` =". $this->id ;

        $dataArray=array($name,$roll,$hobby);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

        if ($singleData)
        {
            Message::message("Data Updated successfully...! ");
        }
        else{
            Message::message("Faild to Update Data...!  ");
        }



    }

}






















