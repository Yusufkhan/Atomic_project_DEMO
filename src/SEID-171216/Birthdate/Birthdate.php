<?php

namespace App\Birthdate;

use PDO,App\Utility\Utility;
use App\Message\Message;
use App\Models\Database;

class Birthdate extends  Database
{
    public $id;
    public $name;
    public $bday;

    public function setBday($Birthday)
    {
        if (array_key_exists("id",$Birthday))
        {
            $this->id = $Birthday['id'];
        }
        if(array_key_exists("Name",$Birthday))
        {
            $this->name = $Birthday['Name'];
        }

        if(array_key_exists("bday",$Birthday))
        {
            $this->bday = $Birthday['bday'];
        }
        return $this;

    }

    public function store()
    {
        $name=$this->name;
        $bday=$this->bday;

        $mysql="INSERT INTO `birthdate` (`name`,`bday`) VALUES (?,?)";

        $dataArray=array($name,$bday);

        $get=$this->conn->prepare($mysql);

        $result=$get->execute($dataArray);

        if($result)
        {
            Message::message("Data inserted successfully");
        }
        else
        {
            Message::message("Error ! Try again ");
        }
    }


    public  function index()
    {
        $mysql=" SELECT * FROM birthdate WHERE is_trashed='No'";
        $get=$this->conn->query($mysql);

        $get->setFetchMode(PDO::FETCH_OBJ);
       // Utility::dd($get);
        $allData=$get->fetchAll();
        return $allData;

    }
    ////////////
    public function view()
    {
        $mysql="SELECT * FROM birthdate WHERE id=".$this->id;
        //Utility::dd($mysql);
        $STH=$this->conn->query($mysql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData= $STH->fetch();
        return $singleData;
    }
    //////////
    public function update()
    {
        $name=$this->name;
        $bday=$this->bday;
        $mysql="UPDATE birthdate SET name = ?,bday = ? WHERE id=".$this->id;
        $dataArray=array($name,$bday);
        $get=$this->conn->prepare($mysql);
        $allData=$get->execute($dataArray);
        return $allData;
    }
    public function trash()
    {
//        $name=$this->name;
//        $bday=$this->bday;
        $mysql="UPDATE birthdate SET is_trashed = NOW() WHERE id=".$this->id;

        $allData=$this->conn->exec($mysql);


        if ($allData){
            Message::message("Data trashed successfully");
        }
       else
       {
           Message::message("failed Data trashed successfully");
       }
    }
    public  function trashed()
    {

        $mysql=" SELECT * FROM birthdate WHERE is_trashed <> 'No'";

        $get=$this->conn->query($mysql);

        $get->setFetchMode(PDO::FETCH_OBJ);

        $allData=$get->fetchAll();
        return $allData;


    }

    public function recover()
    {
//        $name=$this->name;
//        $bday=$this->bday;
        $mysql="UPDATE birthdate SET is_trashed ='No' WHERE id=".$this->id;
        $allData=$this->conn->exec($mysql);

        if ($allData)
        {
            Message::message("Data Recovered Succcessfully");
        }
        else
        {
            Message::message("Failed TO Recover Dta");
        }



    }
    public function delete()
    {


        $mysql="DELETE FROM birthdate WHERE id=".$this->id;
        $allData=$this->conn->exec($mysql);
        if ($allData)
        {
            Message::message("Successfully Deleted");
        }
        else{
           Message::message( "Failed To Delet");
        }

    }
    public function delete_multiple()
    {

        $mysql="DELETE FROM birthdate WHERE id=".$this->id;
        $allData=$this->conn->exec($mysql);
        if ($allData)
        {
            Message::message("Successfully Deleted");
        }
        else{
            Message::message( "Failed To Delet");
        }

    }


}



















