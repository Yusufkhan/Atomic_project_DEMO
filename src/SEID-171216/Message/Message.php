<?php

namespace App\Message;

if (!isset($_SESSION)) session_start();

class Message
{

    public static  function message($message=NULL)
    {
        if (is_null($message)){
            $_message = self::getData();
            return $_message;
        }
        else
        {
            self::setData($message);
        }
    }

    public function setData($message)
    {
        $_SESSION['message']=$message;
    }


    public static function getData()
    {
        if (isset($_SESSION['message'])) $_message=$_SESSION['message'];
        else $_message='';

        $_SESSION['message']='';

        return $_message;
    }


}