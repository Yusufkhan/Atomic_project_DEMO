<?php

namespace App\Textarea;
use App\Message\Message;
use App\Models\Database;
use PDO;

class Textarea extends Database
{
    public $id;
    public $name;
    public $textarea;

    public function setTextarea($textarea)
    {
        if (array_key_exists("id",$textarea))
            $this->id=$textarea['id'];
        if (array_key_exists("name",$textarea))
             $this->name = $textarea['name'];

        if (array_key_exists("textarea",$textarea))
            $this->textarea = $textarea['textarea'];

    }

    public function store()
    {
        $name=$this->name;
        $textarea=$this->textarea;
        $result="INSERT INTO `text_area` (`name`, `textarea`) VALUES ('$name', '$textarea')";
        $mysqlQuery=$this->conn->exec($result);
        if ($mysqlQuery)
        {
            Message::message("Successfully inserted");
        }else
        {
            Message::message("Failed to insert");
        }
    }


    ////////


    public function index()
    {
        $mysql="SELECT  * FROM text_area";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
        // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM text_area WHERE id=".$this->id;
        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        //Utility::dd($read);
        return $singleData;

    }

    public function update()
    {
        $name=$this->name;

        $textarea=$this->textarea;

        $mysql="UPDATE text_area SET name = ?, textarea = ? WHERE id = ".$this->id ;

        $dataArray=array($name,$textarea);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

        if ($singleData)
        {
            Message::message("Data Updated successfully...! ");
        }
        else{
             Message::message("Faild to Update Data...!  ");
        }



    }


}