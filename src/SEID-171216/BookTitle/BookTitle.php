<?php

namespace App\BookTitle;

use App\Message,PDO;
use App\Models\Database;
use App\Utility\Utility;

class BookTitle extends Database
{
    public $id;
    public $booktitle;
    public $authorname;

    public function setData($postArray)
    {
        if (array_key_exists('id',$postArray))
        {
        $this->id=$postArray['id'];
        }

        if (array_key_exists('BookName',$postArray))
        {
           $this->booktitle=$postArray['BookName'];
        }

        if (array_key_exists('AuthorName',$postArray))
        {
            $this->authorname = $postArray['AuthorName'];
        }

    }

    public function store()
    {
        $booktitle=$this->booktitle;
        $authorname=$this->authorname;

        $mysql="INSERT INTO `book_title` (`bookname`,`authorname`) VALUES ('$booktitle', '$authorname')" ;
        $result=$this->conn->exec($mysql);
        if ($result)
        {
            Message\Message::message("Data Inserted successfully...! ");
        }
        else{
            Message\Message::message("Faild to Inserted Data...!  ");
        }
    }

    public function index()
    {
        $mysql="SELECT  * FROM book_title WHERE is_trashed = 'NO' ";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
       // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM book_title WHERE id=".$this->id;
        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        //Utility::dd($read);
        return $singleData;

    }

    public function update()
    {
        $booktitle=$this->booktitle;

        $authorname=$this->authorname;

        $mysql="UPDATE `book_title` SET `bookname` = ?, `authorname` = ? WHERE `id` = $this->id ;";

        $dataArray=array($booktitle,$authorname);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

        if ($singleData)
        {
            Message\Message::message("Data Updated successfully...! ");
        }
        else{
            Message\Message::message("Faild to Update Data...!  ");
        }



    }
    public function trash()

    {
        $mysql="UPDATE book_title SET is_trashed = NOW() WHERE id=".$this->id;
        $get = $this->conn->exec($mysql);
        if ($get)
        {
            Message\Message::message("Successfully trashed");
        }
        else
        {
            Message\Message::message("faild");
        }
    }

    public function trashed()
    {
        $mysql="SELECT  * FROM book_title WHERE is_trashed <>'NO' ";

        $get=$this->conn->query($mysql);

        $get->setFetchMode(PDO::FETCH_OBJ);

        $allData=$get->fetchAll();

        return $allData;
    }
    public function recover()

    {
        $mysql="UPDATE book_title SET is_trashed ='NO' WHERE id=".$this->id;
        $get = $this->conn->exec($mysql);
        if ($get)
        {
            Message\Message::message("Successfully recovered");
        }
        else
        {
            Message\Message::message("faild");
        }
    }

    public function delete()
    {
        $mysql="DELETE FROM book_title WHERE id=".$this->id;
        $get=$this->conn->exec($mysql);

        if ($get)
        {
            Message\Message::message("Successfully Deleted");
        }
        else
        {
            Message\Message::message("Failed to delete");
        }

    }
}
