<?php

namespace App\Gender;
use App\Message;
use App\Models\Database;
use PDO;

class Gender extends Database
{
    public $id;
    public $Name;
    public $Age;
    public $Gender;


    public function setAge($AgeArray)
    {

        if (array_key_exists('id',$AgeArray))
        $this->id =$AgeArray['id'];

        if (array_key_exists('Name',$AgeArray))
            $this->Name =$AgeArray['Name'];

        if (array_key_exists('gender',$AgeArray))
            $this->Gender =$AgeArray['gender'];

        if (array_key_exists('Age',$AgeArray))
            $this->Age =$AgeArray['Age'];

    }


    public function store()
    {
        $Name=$this->Name;
        $Age=$this->Age;
        $Gender=$this->Gender;

        $query="INSERT INTO `gender` (`Name`, `Age`, `gender`) VALUES ('$Name', '$Age', '$Gender');";
        $result=$this->conn->exec($query);

        if ($result)
        {
            Message\Message::message("Data inserted successfully ... :)");

        }
        else
        {
            Message\Message::message("faild to insert data... ");
        }
    }


    public function index()
    {
        $mysql="SELECT  * FROM gender";
        $get=$this->conn->query($mysql);
        $get->setFetchMode(PDO::FETCH_OBJ);
        $allData=$get->fetchAll();
        // Utility::dd($allData);
        return $allData;
    }
    public function view()
    {
        $mysql="SELECT * FROM gender WHERE id=".$this->id;
        $STH=$this->conn->query($mysql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData=$STH->fetch();
        //Utility::dd($read);
        return $singleData;

    }

    public function update()
    {

        $name=$this->Name;
        $age=$this->Age;
        $gnder=$this->Gender;
        $mysql="UPDATE `gender` SET `Name` = ?, `Age` = ? ,`gender`=? WHERE `id` = $this->id ;";

        $dataArray=array($name,$age,$gnder);

        $get=$this->conn->prepare($mysql);

        $singleData=$get->execute($dataArray);

        if ($singleData)
        {
            Message\Message::message("Data Updated successfully...! ");
        }
        else{
            Message\Message::message("Faild to Update Data...!  ");
        }



    }





}