-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2017 at 09:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_b58_s23`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE `birthdate` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bday` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `name`, `bday`) VALUES
(1, 'Yusuf', '31'),
(2, 'cvcv', '2017-06-04'),
(3, 'Yusuf', '1995-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `bookname` varchar(100) NOT NULL,
  `authorname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `bookname`, `authorname`) VALUES
(1, 'Optics', 'Brijlal'),
(3, 'java', 'balaguro'),
(12, 'artificial Inteliengence', 'Rich Knight'),
(13, 'Algorithm', 'Cormen'),
(14, 'java', 'balaguro'),
(15, 'cbdfgb', 'xcb'),
(16, 'yusuf', 'lllll'),
(17, 'yusuf', 'lllll'),
(18, 'hello', 'yusuf'),
(19, 'Computer Network', 'Adil Mahmood'),
(20, 'df', 'ds'),
(21, 'Yusuf', 'balaguro'),
(22, 'Yusuf', 'Khan'),
(23, 'Yusuf', 'balagororshami'),
(24, 'awfwawf', 'Khan'),
(25, 'Yusuf', 'balaguro'),
(26, 'Yusuf', 'balagororshami'),
(27, 'Yusuf', 'balaguro'),
(28, 'Yusuf', 'Khan'),
(29, 'Yusuf', 'balaguro'),
(30, 'Yusuf', 'khan');

-- --------------------------------------------------------

--
-- Table structure for table `city_name`
--

CREATE TABLE `city_name` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `city_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_name`
--

INSERT INTO `city_name` (`id`, `first_name`, `last_name`, `city_name`) VALUES
(1, 'Yusuf', 'Khan', 'Chittagong'),
(2, 'Yusuf', 'Khan', 'Chittagong'),
(3, 'Yusuf', 'khan', 'Dhaka'),
(4, 'Yusuf', 'khan', 'Chittagong'),
(5, 'Yusuf', 'khan', 'Chittagong'),
(6, 'Yusuf', 'Khan', 'Chittagong'),
(7, 'Yusuf', 'khan', 'Chittagong');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Age` int(11) NOT NULL,
  `gender` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `Name`, `Age`, `gender`) VALUES
(6, 'Yusuf', 20, 'Male'),
(7, 'Ismail', 22, 'Male'),
(8, 'afgds', 62, 'Male'),
(9, 'yusuf', 88, 'Male'),
(10, 'yusuf', 62, 'Male'),
(11, 'yusuf', 62, 'Male'),
(12, 'Ismail', 62, 'Male'),
(13, 'yusuf', 62, 'Male'),
(14, 'yusuf', 554, 'Male'),
(15, 'Ismail', 62, 'Male'),
(16, 'dsg', 62, 'Male'),
(17, 'yusuf', 6, 'Male'),
(18, 'yusuf', 62, 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `roll` varchar(10) NOT NULL,
  `hobby` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `roll`, `hobby`) VALUES
(1, 'Yusuf', '171216', ''),
(2, 'dsfasd', '33', 'Cricket,Football,Action,Books'),
(3, 'yusuf', '22', 'Cricket,Historical,Newspaper'),
(4, 'Ismail', '22', 'Cricket, Historical, Newspaper, Books, Megazine'),
(5, 'Yusuf', '20', 'Cricket, Hockye, Action, Commidy, Newspaper, Megazine');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pictures`
--

CREATE TABLE `profile_pictures` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `profile_picture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pictures`
--

INSERT INTO `profile_pictures` (`id`, `name`, `profile_picture`) VALUES
(1, 'f', 'f'),
(2, 'yusuf', '1496217588'),
(3, 'yusuf', '1496217598'),
(4, 'yusuf', '1496217683beauyt.jpg'),
(5, 'yusuf', '1496242666FB_IMG_1495517492308.jpg'),
(6, 'Yusuf', '1496295696beauyt.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `text_area`
--

CREATE TABLE `text_area` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `textarea` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `text_area`
--

INSERT INTO `text_area` (`id`, `name`, `textarea`) VALUES
(2, 'yusuf', '<p>hello everybody ... how are you all</p>'),
(3, 'yusuf', '<p>Hi Iam Yusuf khan ...</p>\r\n<p>&nbsp;</p>'),
(4, 'Yusuf', '<p>Mr yusuf is a student oh iiuc ...</p>'),
(5, '', '<p><strong>vdg&nbsp;</strong></p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_name`
--
ALTER TABLE `city_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `text_area`
--
ALTER TABLE `text_area`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `city_name`
--
ALTER TABLE `city_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profile_pictures`
--
ALTER TABLE `profile_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `text_area`
--
ALTER TABLE `text_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
