<?php
require_once "../../../vendor/autoload.php";

use App\Utility\Utility;

$city = new \App\Textarea\Textarea();

$city->setTextarea($_POST);

$city->update();

Utility::redirect("index.php");