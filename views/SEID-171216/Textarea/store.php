<?php
require_once "../../../vendor/autoload.php";

use App\Utility\Utility;

$text=new \App\Textarea\Textarea();
$text->setTextarea($_POST);
$text->store();

Utility::redirect("create.php");