<?php
require_once "../../../vendor/autoload.php";
if (!isset($_SESSION)) session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src='../../../resources/tinymce/js/tinymce/tinymce.min.js'></script>
    <link rel="stylesheet" href="../../../Css/css.css">
    <style >
        body{
            background-image: url("../../../resources/Images/Background-1.jpg");
            background-repeat: no-repeat;
        }
    </style>
</head>
<body>

<div style="background-color: transparent">

    <nav class="navbar"  style="background-image: url(../../../resources/Images/Border-2.jpg) " >
        <div class="container-fluid">

            <div class="dropdown">
                <button class="dropbtn">Birthdate</button>
                <div class="dropdown-content">
                    <a href="../Birthdate/create.php">Create</a>
                    <a href="../Birthdate/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Textarea</button>
                <div class="dropdown-content">
                    <a href="../Textarea/create.php">Create</a>
                    <a href="../Textarea/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Book Title</button>
                <div class="dropdown-content">
                    <a href="../book_title/create.php">Create</a>
                    <a href="../book_title/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Gender</button>
                <div class="dropdown-content">
                    <a href="../Gender/create.php">Create</a>
                    <a href="../Gender/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Hobby</button>
                <div class="dropdown-content">
                    <a href="../Hobby/create.php">Create</a>
                    <a href="../Hobby/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">ProfilePicture</button>
                <div class="dropdown-content">
                    <a href="../ProfilePicture/create.php">Create</a>
                    <a href="../ProfilePicture/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">City</button>
                <div class="dropdown-content">
                    <a href="../City/create.php">Create</a>
                    <a href="../City/index.php">Index</a>
                </div>
            </div>
            <ul class="nav navbar-nav" >
                <li><a href="" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="form-group">
<form role="form" action="store.php" method="post">
    <div class="container">
        <h2 style="color: white;">Textarea</h2>

        <div class="form-group">
            <label style="color: white"> Enter Your Organizations Name:</label>
            <input type="text" name="name" class="form-control"  placeholder="Enter Your Organizations Name" style="background-color: transparent"></br>

            <label for="comment" style="color: white">Enter your summary :</label>
            <textarea name="textarea" class="form-control" rows="6" id="mytextarea" style="background-color: transparent"></textarea>
            <input type="submit" class="btn btn-primary" style="color: white;margin-top: 3%">
        </div>
        <div class="form-group">

                <?php
                use App\Message\Message;

                $message = Message::message();

                echo "
                    <div id='message' style='color: white; alignment=center' class='form-group'>
                    <h3>$message</h3>
                    </div>";
                ?>

        </div>
        <script src="../../../resources/bootstrap/js/jquery.js"></script>
        <script>
            jQuery(
                function($)
                {
                    $('#message').fadeOut (550);
                    $('#message').fadeIn (550);
                    $('#message').fadeOut (550);
                    $('#message').fadeIn (550);
                    $('#message').fadeOut (550);
                    $('#message').fadeIn (550);
                    $('#message').fadeOut (550);
                }
            )
        </script>
</form>
</div>
<script>
    tinymce.init({
        selector: '#mytextarea'
    });
</script>

</body>
</html>

