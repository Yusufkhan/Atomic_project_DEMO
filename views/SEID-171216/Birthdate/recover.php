<?php
require_once "../../../vendor/autoload.php";
use App\Utility\Utility;

use App\Message\Message;

$obj = new \App\Birthdate\Birthdate();

$obj->setBday($_GET);

$obj->recover();

Utility::redirect("index.php");