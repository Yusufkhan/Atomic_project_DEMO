<?php
require_once "../../../vendor/autoload.php";

$object = new \App\Birthdate\Birthdate();

$object->setBday($_GET);

$singleData = $object->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
</head>
<body>

<div style="background-color: green">

    <nav class="navbar" style="background-color:green " >
        <div class="container-fluid">

            <ul class="nav navbar-nav" >
                <li class="dropdown"><a href="" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li><a href="index.php" style="color: white">Index</a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="table">

    <table class="table table-bordered table-striped">

        <tr>
            <th>Id</th>
            <th> Name</th>
            <th>Date of Birth</th>
        </tr>

        <?php
        echo "
            <tr>
                <td>$singleData->id</td>
                <td>$singleData->name</td>
                <td>$singleData->bday</td>
   
            </tr>
        ";

        ?>
    </table>


</div>
</body>
</html>
