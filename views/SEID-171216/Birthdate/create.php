<?php
require_once "../../../vendor/autoload.php";
if (!isset($_SESSION)) session_start();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../Css/css.css">

    <style>
        body{
            background-attachment: fixed;
            background-image: url("../../../resources/Images/Background-1.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }

        .col-md-8{
            background-color: transparent;
            margin-top: 50px;
            border-radius: 25px;
            color:white;
        }

        .form-control{
            border-radius: 15px;
            background-color: transparent;

        }

        .btn{
            background-color: #20a815;
            color:white;
            margin-top: 10px;
            border-radius: 15px;
        }

    </style>

</head>
<body>

<div style="background-color: green">

    <nav class="navbar" style="background-image: url(../../../resources/Images/Border-2.jpg) " >
        <div class="container-fluid">

            <div class="dropdown">
                <button class="dropbtn">Birthdate</button>
                <div class="dropdown-content">
                    <a href="../Birthdate/create.php">Create</a>
                    <a href="../Birthdate/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Textarea</button>
                <div class="dropdown-content">
                    <a href="../Textarea/create.php">Create</a>
                    <a href="../Textarea/create.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Book Title</button>
                <div class="dropdown-content">
                    <a href="../book_title/create.php">Create</a>
                    <a href="../book_title/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Gender</button>
                <div class="dropdown-content">
                    <a href="../Gender/create.php">Create</a>
                    <a href="../Gender/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Hobby</button>
                <div class="dropdown-content">
                    <a href="../Hobby/create.php">Create</a>
                    <a href="../Hobby/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">ProfilePicture</button>
                <div class="dropdown-content">
                    <a href="../ProfilePicture/create.php">Create</a>
                    <a href="../ProfilePicture/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">City</button>
                <div class="dropdown-content">
                    <a href="../City/create.php">Create</a>
                    <a href="../City/index.php">Index</a>
                </div>
            </div>
            <ul class="nav navbar-nav" >
                <li><a href="../../../Front-Page.php" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <div class="col-md-2"> </div>
    <div class="col-md-8">

        <form action="store.php" method="post">
            <div class="col-md-6" style="margin-top: 10%;margin-left: 30%">


                <div class="form-group">
                    <label for="Name">Name:</label>
                    <input type="text" class="form-control" id="Name" required name="Name" placeholder="Enter Name" style="color: white">
                </div>


                <div class="" style="text-align: center">

                </div>
                <div class="" style="text-align: center" >
                    <strong  >Select Birthdate :</strong>
                    <input type="date" name="bday" style="color: #0f0f0f">

                </div>


                <div align="center">
                    <button type="submit" class="btn btn-success"><b>Submit</b></button>
                </div>
                <div>
                    <?php
                    use App\Message\Message;

                    $msg = Message::message();

                    echo "
                    <div id='message' style='color: green; alignment=center' class='form-group'>
                    <h3>$msg</h3>
                    </div>";
                    ?>
                </div>


        </form>
        <div class="" style="alignment: center;color: white">

            <script src="../../../resources/bootstrap/js/jquery.js"></script>
            <script>
                jQuery(
                    function($)
                    {
                        $('#message').fadeOut (550);
                        $('#message').fadeIn (550);
                        $('#message').fadeOut (550);
                        $('#message').fadeIn (550);
                        $('#message').fadeOut (550);
                        $('#message').fadeIn (550);
                        $('#message').fadeOut (550);
                    }
                )
            </script>
        </div>
    </div>
    </div>
    <div class="col-md-2"> </div>
</div>
</body>
</html>
