<?php
require_once "../../../vendor/autoload.php";

use App\Birthdate;

use App\Message\Message;

$obj = new Birthdate\Birthdate();

$allData = $obj->index();

$message=Message::message();

echo "<div style='alignment: center;background-color: yellow' id='message' > $message</div>"

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
    <style>
        .nav > li > a:hover
        {

            background-color: #35a741;
        }
        .checkbox
    </style>
</head>
<body>

<div style="background-color: green">

    <nav class="navbar" style="background-color:transparent " >
        <div class="container-fluid">

            <ul class="nav navbar-nav" >
                <li class="dropdown"><a href="" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li><a href="create.php" style="color: white">Home</a></li>
                <li><a href="trashed.php" style="color: white">Trashed List</a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>

<div><marquee behavior="alternate " direction="up"><h2 align="center">Active List of Birthdate</h2></marquee </div>

<form id="select" action="trash_multiple.php" method="post" name="select" >

<!--    <input type="submit" value="Trash_All" class="btn btn-warning">-->
<!--    <button href="delete_multiple.php" onclick=" return confirm_delete()" id="deleteall" class="btn btn-danger">Delete All</button>-->
    <div class="" style="margin-left: %">
        <input type="submit" value="Trash_All" class="btn btn-warning">
        <button href="delete_multiple.php" onclick=" return confirm_delete()" id="deleteall" class="btn btn-danger">Delete All</button>
    </div>


<table class="table table-bordered table-striped" style="margin-top: 1em">

    <tr>
        <th>Select All <input type='checkbox' name='all' id="all" class='checkbox' value='$record->id'></th>
        <th>Serial</th>
        <th>Id</th>
        <th> Name</th>
        <th> Birthdate</th>
        <th>Action Button</th>
    </tr>
    <?php
    $serial=1;
    foreach ($allData as $record)
    {
        echo "
                <tr>
                    <td><input type='checkbox' name='all[]' class='checkbox' value='$record->id'></td>
                    <td>$serial</td>
                    <td>$record->id</td>
                    <td>$record->name</td>
                    <td>$record->bday</td>
                    <td>
                        <a href='view.php?id=$record->id' class='btn btn-primary'>View</a>
                        <a href='edit.php?id=$record->id' class='btn btn-success'>Edit</a>
                        <a href='trash.php?id=$record->id' class='btn btn-warning'>Trash</a>
                        <a href='delete.php?id=$record->id' class='btn btn-danger' onclick='return confirm_delete()'>Delete</a>
                    </td>
                </tr>
             ";
        $serial++;
    }
    ?>
</table>

</form>
<div class="" style="alignment: center;color: white">

    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script>
        jQuery(
            function($)
            {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</div>

<!--<script>-->
<!--    function delete_all() {-->
<!--        return confirm("Are You Sure?");-->
<!--    }-->
<!--</script>-->

<script>
    function confirm_delete() {
      return confirm("Are You Sure?");
    }
</script>

<script>


    $('#deleteall').click(function(){
        document.getElementById("select").action ="delete_multiple.php";
        $('#select').submit();
    });

</script>
<script>

    //select all checkboxes
    $("#all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>


</body>
</html>
