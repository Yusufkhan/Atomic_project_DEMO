<?php
require_once "../../../vendor/autoload.php";
use App\Utility\Utility;

use App\Message\Message;

$obj = new \App\BookTitle\BookTitle();

$Ids = $_POST['checkbox'];

foreach ($Ids as $id)
{
    $_GET['id']=$id;
    $obj->setData($_GET);

    $obj->recover();
}

Utility::redirect("index.php");