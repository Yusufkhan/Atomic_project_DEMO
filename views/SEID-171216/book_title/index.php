<?php
require_once "../../../vendor/autoload.php";

$obj=new \App\BookTitle\BookTitle();
$allData=$obj->index();

use App\Message\Message;

$message=Message::message();

echo "<div class='' id='message'>$message </div>";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
    <style>
        .nav > li > a:hover
        {
            background-color: #1c807e;
        }
    </style>
</head>
<body>

<div style="background-color: green">

    <nav class="navbar" style="background-color:transparent " >
        <div class="container-fluid">

            <ul class="nav navbar-nav" style="">
                <li class="dropdown" ><a href="create.php" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li ><a href="trashed.php" style="color: white">Trshed List</a></li>
                <li><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>

           
        </div>
    </nav>
</div>

<div><marquee  behavior="alternate " direction="up"><h2 align="center" style="color: green">Active List of Book Title</h2></marquee </div>

<form action="trash_multiple.php" method="post" id="select">
    <div class="" style="margin-bottom: 2%">

        <input type="submit" href="trash_multiple.php" value="Trash All" class="btn btn-info" >
        <button href="delete_multiple.php" id="deleteall" value="Delete All" class="btn btn-danger" onclick="return confirm_delete()" >Delete All</button>

    </div>
<table class="table table-bordered table-striped">

    <tr>
        <th>Select All <input type="checkbox" name="checkbox[]"  class="checkbox"  value='$record->id' id="all" ></th>
        <th>Serial</th>
        <th>Id</th>
        <th>Book Title</th>
        <th>Author Name</th>
        <th>Action Button</th>
    </tr>
    <?php
    $serial=1;
        foreach ($allData as $record)
        {
            echo "
                <tr>
                    <td><input type='checkbox' id='all' value='$record->id' class='checkbox' name='checkbox[]' ></td>
                    <td>$serial </td>
                    <td>$record->id</td>
                    <td>$record->bookname</td>
                    <td>$record->authorname</td>
                    <td>
                        <a href='view.php?id=$record->id' class='btn btn-primary'>View</button></a>
                        <a href='edit.php?id=$record->id'  class='btn btn-success'>Edit</button></a>
                        <a href='trash.php?id=$record->id'  class='btn btn-warning'>Trash</button></a>
                        <a href='delete.php?id=$record->id' onclick='return confirm_delete()' class='btn btn-danger'>Delete</button></a>
                       
                    </td>
                </tr>
             ";
            $serial++;
        }
    ?>
</table>
</form>
<div>
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <script>
        jQuery(
            function($)
            {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>

    <script>
        function confirm_delete() {
            return confirm("Are you sure ??");
        }
    </script>
    <script>


        $('#deleteall').click(function()
        {
            document.getElementById("select").action ="delete_multiple.php";
            $('#select').submit();
        });

    </script>


    <script>

        //select all checkboxes
        $("#all").change(function(){  //"select all" change
            var status = this.checked; // "select all" checked status
            $('.checkbox').each(function(){ //iterate all listed checkbox items
                this.checked = status; //change ".checkbox" checked status
            });
        });

        $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){ //if this item is unchecked
                $("#all")[0].checked = false; //change "select all" checked status to false
            }

//check "select all" if all checkbox items are checked
            if ($('.checkbox:checked').length == $('.checkbox').length ){
                $("#all")[0].checked = true; //change "select all" checked status to true
            }
        });



    </script>
</div>
</body>
</html>
