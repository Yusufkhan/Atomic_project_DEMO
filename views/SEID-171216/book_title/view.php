<?php
require_once "../../../vendor/autoload.php";

$obj = new \App\BookTitle\BookTitle();

$obj->setData($_GET);

$singleData=$obj->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
</head>
<body>
<div class="navbar" style="color: white">

    <nav class="navbar navbar-fluid" style="background-color:transparent ">
        <div class="container-fluid">

            <ul class="nav navbar-nav" style="margin-left: 10%">
                <li><a href="index.php">Index</a></li>
                <li><a href="create.php">Create</a></li>
                <li><a href="">Edit</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="table">

    <table class="table table-bordered table-striped">

        <tr>
            <th>Id</th>
            <th>Book Name</th>
            <th>Author Name</th>
        </tr>

        <?php
        echo "
            <tr>
                <td>$singleData->id</td>
                <td>$singleData->bookname</td>
                <td>$singleData->authorname</td>
   
            </tr>
        ";

        ?>
    </table>


</div>
</body>
</html>
