<?php
require_once "../../../vendor/autoload.php";

use App\Utility\Utility;

$check = new \App\Hobby\Hobby();


//Utility::dd($_POST);

$selected_hobby = $_POST['hobby'];

$compact=implode(", " ,$selected_hobby);

$_POST['hobby']=$compact;

$check->setHobby($_POST);

$check->store();

Utility::redirect("create.php");