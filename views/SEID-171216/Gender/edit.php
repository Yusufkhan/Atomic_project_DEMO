<?php
require_once "../../../vendor/autoload.php";

if (isset($_SESSION)) session_start();

use App\Gender\Gender;
use App\Message\Message;
$obj = new Gender();

$obj->setAge($_GET);

$singleData=$obj->view();

$message = Message::message();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>

    <style>
        body{
            background-attachment: fixed;
            background-image: url("../../../resources/Images/book.jpg");
            background-repeat: no-repeat;


        }

        .col-md-8{
            background-color: transparent;
            color: white;
            margin-top: 50px;
            border-radius: 25px;
        }

        .form-control{
            border-radius: 15px;
            background-color: transparent;
            color: white;
        }

        .btn{
            background-color: #20a815;
            color:white;
            margin-bottom: 10px;
            border-radius: 15px;
        }

         .nav > li > a:hover
         {
             background-color: #1c807e;
         }


    </style>

</head>
<body>
<div class="container">
    <div class="navbar" style="margin-left: 35%">

        <nav class="navbar navbar-static-top" style="background-color:transparent ">
            <div class="container-fluid">

                <ul class="nav navbar-nav" style="margin-left: 10%">
                    <li><a href="index.php" style="color: white">Index</a></li>
                    <li><a href="create.php" style="color: white">Create</a></li>
                    <li><a href="edit.php" style="color: white">Edit</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="col-md-2"> </div>
    <div class="col-md-8">

        <form action="update.php" method="post">
            <div class="col-md-6" style="margin-top: 5%;margin-left: 30%">

                <div class="form-group">
                    <label for="Name">Name:</label>
                    <input type="text" value="<?php echo $singleData->Name?>" class="form-control" id="Name" required name="Name" placeholder="Enter BookName  ">
                </div>

                <div class="form-group">
                    <label for="Age">Age :</label>
                    <input type="text" class="form-control" value="<?php echo $singleData->Age?>" required id="Age" name="Age" placeholder="Enter Author Name ">
                </div>
                <div class="form-group">
                    <label for="gender">Gender :</label>
                    <input type="text" class="form-control" value="<?php echo $singleData->gender?>" required id="gender" name="gender" placeholder="Enter Author Name ">
                </div>

                <div align="center">
                    <input type="hidden" name="id" value="<?php echo $singleData->id?>">
                    <input type="submit" class="btn btn-success" value="Update">
                </div>
            </div>
        </form>



    <div class="col-md-2"> </div>

</div>

</body>
</html>