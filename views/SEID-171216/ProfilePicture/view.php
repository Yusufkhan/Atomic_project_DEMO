<?php
require_once "../../../vendor/autoload.php";
$obj = new  \App\ProfilePicture\ProfilePicture();
$obj->setImage($_GET);
$singleData=$obj->view();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <style>
        .nav > li > a:hover{
            background-color: #5bc0de;
        }
    </style>
</head>
<body>
<div style="background-color: green">
    <nav class="navbar" style="background-color:transparent " >
        <div class="container-fluid">

            <ul class="nav navbar-nav" style="margin-left: 35%">
                <li><a href="create.php" style="color: white">Create</a></li>
                <li ><a href="edit.php" style="color: white">Edit</a></li>
            </ul>
        </div>
    </nav>
</div>
    <table class="table table-bordered table-striped">

        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Profile Picture</th>
        </tr>

        <?php
        echo "<tr>
                 <td>$singleData->id</td>
                 <td>$singleData->name</td>
                 <td><img src='images/$singleData->profile_picture' width='100px;' height='100px;'></td>
            </tr>";

        ?>

    </table>
</body>
</html>
