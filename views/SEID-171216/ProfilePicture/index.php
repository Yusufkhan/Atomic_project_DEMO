<?php
require_once "../../../vendor/autoload.php";

$obj = new \App\ProfilePicture\ProfilePicture();

$allData=$obj->index();
use App\Message\Message;

$message=Message::message();

echo "

<div class='' id='message' style='text-align: center;background-color: yellow'>$message </div>";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
    <style>
        .nav > li > a:hover
        {
            background-color: #1c807e;
        }
    </style>
</head>
<body>

<div style="background-color: green">
    <nav class="navbar" style="background-color:transparent " >
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class="dropdown"><a href="" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li><a href="create.php" style="color: white">Create</a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>

<table class="table table-bordered table-striped">


    <tr>
        <th>Serial</th>
        <th>Id</th>
        <th>Name</th>
        <th>Profile Picture</th>
        <th>Action Button</th>
    </tr>
    <?php
    $serial=1;
    foreach ($allData as $record)
    {
        echo "
                <tr>
                    <td>$serial</td>
                    <td>$record->id</td>
                     <td>$record->name</td>
                 
                    <td><img src='images/$record->profile_picture' width='100px;' height='100px;'></td>
   
              
                    <td>
                        <a href='view.php?id=$record->id'>  <button value='view' class='btn btn-primary'>View</button></a>
                        <a href='edit.php?id=$record->id'><button value='edit' class='btn btn-success'>Edit</button></a>
                    </td>
                </tr>
             ";
        $serial++;
    }
    ?>
</table>>

</body>
</html>











