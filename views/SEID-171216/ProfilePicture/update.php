<?php
require_once "../../../vendor/autoload.php";

use App\Utility\Utility;

$obj= new \App\ProfilePicture\ProfilePicture();

$fileName=time().$_FILES['image']['name'];

$source=$_FILES['image']['tmp_name'];

$destination="images/".$fileName;

move_uploaded_file($source,$destination);

$_POST['profilePicture']=$fileName;

$obj->setImage($_POST);

$obj->update();

Utility::redirect("index.php");