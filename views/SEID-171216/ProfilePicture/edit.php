<?php
require_once "../../../vendor/autoload.php";

if (isset($_SESSION)) session_start();

use App\ProfilePicture\ProfilePicture;
 $obj= new ProfilePicture();

$obj->setImage($_GET);
$singleData=$obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <style>
        body{
            background-attachment: fixed;
        }
        .col-md-8{

            margin-top: 50px;
            border-radius: 25px;
        }
        .form-control{
            border-radius: 15px;

        }
        .btn{
            background-color: #20a815;

            margin-bottom: 10px;
            border-radius: 15px;
        }
         .nav > li > a:hover
         {
             background-color: #1c807e;
         }

    </style>
</head>
<body>

<div style="background-color: green">
    <nav class="navbar" style="background-color:transparent " >
        <div class="container-fluid">
            <ul class="nav navbar-nav" style="margin-left: 35%">
                <li><a href="create.php" style="color: white" class="glyphicon glyphicon-home"></a></li>
                <li><a href="create.php" style="color: white">Create</a></li>
                <li ><a href="edit.php" style="color: white">Edit</a></li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <div class="col-md-2"> </div>
    <div class="col-md-8">

        <form action="update.php" method="post" enctype="multipart/form-data">
            <div class="col-md-6" style="margin-top: 5%;margin-left: 30%">

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" value="<?php echo $singleData->name?>" class="form-control" id="name" required name="name" placeholder="">
                </div>

                <div class="form-group">
                    <label for="image">Picture :</label>
                    <input type="file" class="form-control"  value="<?php echo $singleData->profile_picture?>" required id="image" name="image" placeholder=" ">
                </div>

                <div align="center">
                    <input type="hidden" name="id" value="<?php echo $singleData->id?>">
                    <input type="submit" class="btn btn-success" value="Update">
                </div>
            </div>
        </form>

    <div class="col-md-2"> </div>

</div>

</body>
</html>