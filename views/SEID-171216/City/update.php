<?php
require_once "../../../vendor/autoload.php";

use App\Utility\Utility;

$city = new \App\City\City();

$city->setCityname($_POST);

$city->store();

Utility::redirect("index.php");