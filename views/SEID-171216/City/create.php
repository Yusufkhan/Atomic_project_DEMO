<?php
require_once "../../../vendor/autoload.php";

if (!isset($_SESSION)) session_start();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../Css/css.css">

    <style>
        body{
            background-image: url("../../../resources/Images/th1.jpg");
            background-repeat: no-repeat;
            background-attachment: fixed;


        }

        .col-md-8{
            background-color: transparent;
            margin-top: 50px;
            border-radius: 25px;
            color:white;
        }

        .form-control{
            border-radius: 15px;
            background-color: transparent;
            color: rgba(27, 74, 143, 0.21);

        }

        .btn{
            background-color: #20a815;
            color:white;
            margin-top: 10px;
            border-radius: 15px;
        }

    </style>

</head>
<body>

<div style="background-color: transparent">

    <nav class="navbar"  style="background-image: url(../../../resources/Images/Border-2.jpg) " >
        <div class="container-fluid">

            <div class="dropdown">
                <button class="dropbtn">Birthdate</button>
                <div class="dropdown-content">
                    <a href="../Birthdate/create.php">Create</a>
                    <a href="../Birthdate/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Textarea</button>
                <div class="dropdown-content">
                    <a href="../Textarea/create.php">Create</a>
                    <a href="../Textarea/create.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Book Title</button>
                <div class="dropdown-content">
                    <a href="../book_title/create.php">Create</a>
                    <a href="../book_title/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Gender</button>
                <div class="dropdown-content">
                    <a href="../Gender/create.php">Create</a>
                    <a href="../Gender/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">Hobby</button>
                <div class="dropdown-content">
                    <a href="../Hobby/create.php">Create</a>
                    <a href="../Hobby/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">ProfilePicture</button>
                <div class="dropdown-content">
                    <a href="../ProfilePicture/create.php">Create</a>
                    <a href="../ProfilePicture/index.php">Index</a>
                </div>
            </div>
            <div class="dropdown">
                <button class="dropbtn">City</button>
                <div class="dropdown-content">
                    <a href="../City/create.php">Create</a>
                    <a href="../City/index.php">Index</a>
                </div>
            </div>
            <ul class="nav navbar-nav" >
                <li><a href="" class="glyphicon glyphicon-home" style="color:white;"></a></li>
                <li ><a href="" style="color: white" class="glyphicon glyphicon-refresh"></a></li>
            </ul>
        </div>
    </nav>
</div>

<div class="container">
    <div class="col-md-2"> </div>
    <div class="col-md-8">

        <form action="store.php" method="post">
            <div class="col-md-6" style="margin-top: 10%;margin-left: 30%">

                <div class="form-group">
                    <label for="fname">First Name:</label>
                    <input type="text" class="form-control" id="fname" required name="fname" placeholder="Enter Name" style=" color: white;background-color: rgba(27, 74, 143, 0.21)">
                </div>

                <div class="form-group">
                    <label for="lname"> Last Name :</label>
                    <input type="text" class="form-control"  required id="lname" name="lname" placeholder="Last Name " style=" color: white ; background-color: rgba(27, 74, 143, 0.21)">
                </div>


                <div class="form-group">
                    <label for="cityname" >Select your city name:</label>
                    <select id="cityname" class="form-control" name="cityname" style="background-color: rgba(27, 74, 143, 0.21);color: #122b40">
                        <option >CIty</option>
                        <option>Dhaka</option>
                        <option>Chittagong</option>
                        <option>Khulna</option>
                        <option>Rajshahi</option>
                        <option>Barisal</option>
                        <option>Rangpur</option>
                        <option>Comilla</option>
                        <option>Sylhet</option>
                    </select>
                </div>

                <div align="center" class="">
                    <button type="submit" class="btn btn-success"><b>Submit</b></button>
                </div>
                <div>
                    <?php
                    use App\Message\Message;

                    $message = Message::message();

                    echo "
                    <div id='message' style='color: green; alignment=center' class='form-group'>
                    <h3>$message</h3>
                    </div>";
                    ?>
                </div>

        </form>

                <div>
                    <script src="../../../resources/bootstrap/js/jquery.js"></script>
                    <script>
                        jQuery(
                            function($)
                            {
                                $('#message').fadeOut (550);
                                $('#message').fadeIn (550);
                                $('#message').fadeOut (550);
                                $('#message').fadeIn (550);
                                $('#message').fadeOut (550);
                                $('#message').fadeIn (550);
                                $('#message').fadeOut (550);
                            }
                        )
                    </script>
                </div>
            </div>

    </div>
    <div class="col-md-2"> </div>

</body>
</html>
