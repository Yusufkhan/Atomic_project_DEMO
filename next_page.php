<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic_project_name</title>

    <!-- Bootstrap -->
    <link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body{
            background-image: url("images/Background-1.jpg");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }
        .navbar{
            background-image: url("images/Background-1.jpg");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

        img{
            width: 50%;
            margin-top: 5px;
            margin-left: -150px;
        }
        .group-name{
            font-family: "Tempus Sans ITC";
            float: right;
            font-size: 35px;
            font-weight: bold;

        }
        .col-sm-8{
            background-image: url("images/Border-1.jpg");
            border-radius: 30px;
            margin-top: 20px;
            border: 1px inset #005EE9
        }
        .navbar-collapse{
            margin-top: 20px;
            background-image: url("images/Border-1.jpg");
            border-radius: 30px;
            border: 1px inset #005EE9;
        }
        h1{
            font-family: "Tempus Sans ITC";
            font-weight: bold;
            color: #2b76b7;
            border: 1px inset #005EE9;
            background-image: url("images/Border-2.jpg");
            border-radius: 30px;
            text-align: center;
            padding: 5px;
        }

        button{
            border: 2px outset #005EE9;
            padding: 10px 60px 10px 60px;
            margin: 25px;
            border-radius: 30px;
            color: white;
            font-weight: bold;
        }

        h4{
            font-family: "Tempus Sans ITC";
            font-weight: bold;
            color: ghostwhite;
            border: 1px inset black;
            border-radius: 30px;
            text-align: center;
            padding: 5px;
            background-image: url("images/Border-1.jpg");
        }

    </style>
</head>
<body>
    <nav class="navbar">
        <div class="container">

            <div class="navbar-header col-md-9 col-sm-9 col-xs-9">
                <div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <a href = '#' class="logo"><img src = 'images/logo.jpg'/></a>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <a class="navbar-brand group-name" href="#" style="margin-right: 4em">PHP KILLER</a>
                    </div>
                </div>
            </div>

            <div class="collapse navbar-collapse col-md-3 col-sm-3 col-xs-3 pull-right">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://localhost/PHP-Killer_Atomic-Project/Front-Page.php?_ijt=pnmkttdmmj41fubk5kd17s3t8k"><b>Home</b><span class="sr-only">(current)</span></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <h1>Atomic Project</h1>
        <div class="container-fluid" align="center">

            <div class="col-md-4">
                <button type="button" href="" style="background-color: #bc97ee;background-image: url('images/Background-5.gif')"> <a href="views/SEID-171216/book_title/create.php">Book Title</a></button>
            </div>
            <div class="col-md-4">
                <button type="button" style="background-color: #5CB85C;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/Birthdate/create.php">Birthdate</a></button>
            </div>
            <div class="col-md-4">
                <button type="button" style="background-color: #F0AD4E;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/City/create.php">City</a></button>
            </div>
        </div>

        <div class="container-fluid" align="center">
            <div class="col-md-3">
                <button type="button" style="background-color: #5BC0DE;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/Gender/create.php">Gender</a></button>
            </div>
            <div class="col-md-3">
                <button type="button" style="background-color: #dfbd7c;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/Hobby/create.php">Hobby</a></button>
            </div>
            <div class="col-md-3">
                <button type="button" style="background-color: #8d80df;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/ProfilePicture/create.php">Profile Picture</a></button>
            </div>
            <div class="col-md-3" >
                <button type="button" style="background-color: #D9534F;background-image: url('images/Background-5.gif')"><a href="views/SEID-171216/Textarea/create.php">Textarea</a></button>
            </div>
        </div>
    </div>

    <div class="container-fluid navbar-fixed-bottom">
        <div class="col-md-4"></div>

        <div class="col-md-4" align="center">
            <h4>Designed By- PHP KILLER</h4>
        </div>

        <div class="col-md-4"></div>

    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>