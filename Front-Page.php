<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">



    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body{
            background-image: url("images/Background-1.jpg");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }
        .navbar{
            background-image: url("images/Background-1.jpg");
            background-size: cover;
            background-attachment: fixed;
            background-repeat: no-repeat;
        }

        img{
            width: 50%;
            margin-top: 5px;
            margin-left: -150px;
        }
        .group-name{
            font-family: "Tempus Sans ITC";
            float: right;
            font-size: 35px;
            font-weight: bold;

        }

        .navbar-collapse{
            margin-top: 20px;
            background-image: url("images/Border-1.jpg");
            border-radius: 30px;
            border: 1px inset #005EE9;
        }

        .col-sm-8{
            background-image: url("images/Border-1.jpg");
            border-radius: 30px;
            margin-top: 20px;
            border: 1px inset #005EE9;

        }
        h1{
            font-family: "Tempus Sans ITC";
            font-weight: bold;
            color: #2b76b7;
            border: 1px inset #005EE9;
            background-color: #222325;
            border-radius: 30px;
            text-align: center;
            padding: 5px;
            background-image: url("images/Border-2.jpg");

        }

        .Group-member{
            color: white;
            margin-top: 30px;
            font-weight: bold;
            font-size: large;
        }

        .member ul li{
            border: 2px outset #005ee9;

            padding: 5px;
            margin: 25px;
            color: #2b76b7;
            border-radius: 30px;
            background-image: url("images/Background-5.gif");
            text-shadow: #2aabd2;
            box-shadow: #005ee9;
        }

        .member ul li:hover{
            color: ghostwhite;
            transition: .5s;
        }

        h3{

            color: #2b76b7;
            border: 1px outset #005EE9;
            border-radius: 30px;
            text-align: center;
            padding: 5px;
            background-color: transparent;
            background-image: url("images/Background-5.gif");
        }

        h3:hover{
            color: #1A1A1A;
            transition: .5s;
        }

        h4{
            font-family: "Tempus Sans ITC";
            font-weight: bold;
            color: ghostwhite;
            border: 1px inset black;
            border-radius: 30px;
            text-align: center;
            padding: 5px;
            background-image: url("images/Border-1.jpg");
        }




    </style>

</head>
<body>

<nav class="navbar">
    <div class="container">

        <div class="navbar-header col-md-9 col-sm-9 col-xs-9">
            <div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <a href = '#' class="logo"><img src = 'images/logo.jpg'/></a>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <a class="navbar-brand group-name" href="#" style="margin-right: 4em">PHP KILLER</a>
                </div>
            </div>


        </div>

        <div class="collapse navbar-collapse col-md-3 col-sm-3 col-xs-3 pull-right">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="next_page.php"><b>Atomic Project</b><span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <h1>Group-Member</h1>
    <div class="container-fluid Group-member">

        <div class="col-md-4 col-sm-4 col-xs-4 member" align="center">
            <ul>
                <li>
                    Md. Shamim Abullah bin Kafi
                    <br>
                    SEID:172199
                </li>
                <li>
                    Yusuf Bin Nur
                    <br>
                    SEID:171216
                </li>
                <li>
                    Rihan Subhan
                    <br>
                    SEID:172199
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4"></div>

        <div class="col-md-4 col-sm-4 col-xs-4 member" align="center">
                <ul>
                    <li>
                        Sharif Khan
                        <br>
                        SEID:172199
                    </li>
                    <li>
                        Shah Md. Rizve
                        <br>
                        SEID:172199
                    </li>
                    <li>
                        Tanzim Ahmed
                        <br>
                        SEID:172199
                    </li>
                </ul>
        </div>

    </div>
</div>




<div class="container-fluid navbar-fixed-bottom">
    <div class="col-md-4"></div>

    <div class="col-md-4" align="center">
        <h4>Designed By- PHP KILLER</h4>
    </div>

    <div class="col-md-4"></div>

</div>

</body>
</html>